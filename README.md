**ASAP** is a set of audio plug-ins that *takes over and adapts* some of the features of *AudioSculpt* in order to be used in digital audio workstations. You are invited to *play with the sound representation and the synthesis parameters* in order to generate new sounds. The plug-ins can also be used in order to *correct the defaults of the sound* and to *improve audio rendering*. Thanks to the ARA2 integration, the *spectral transformations are integrated into your editing workflow*. The plugins are based on the SuperVP audio engine developed by the Ircam analysis-synthesis team.   
Check out the [ASAP tutorials](https://forum.ircam.fr/article/detail/asap-tutorials/) and video presentations below!

![Overview](https://git.forum.ircam.fr/guillot/asap/uploads/cb6af8fc129c36bd945901dbb7656774/Overview.gif)   
Discover the images gallery below!
  
ㅤ  
The **ASAP** plugins set contains:  
• **Spectral Remix** [Free]: The plug-in allows you to control the balance of the *harmonic, noise, and attack components* of the sound. Beyond many original approaches, the plug-in can be used to *highlight or hide certain audio elements and characteristics* such as background noise, vocals, percussive sounds, and so on.  
• **Spectral Clipping** [Free]: The plug-in allows you to *expand and compress the energy of spectral components* within a range of thresholds. It can be used to *silent low-level sounds* such as background noise or to *limit high energy peaks* such as high-pitched bird calls.  
• **Spectral Crossing** [Premium]: The plug-in allows you to *cross the amplitudes and frequencies of a source sound and of a side-chain sound* to *generate a hybrid* sound. The plug-in can be used to creatively interpolate and transform one sound into another by gradually mixing the phase and amplitude components of the two audio signals.  
• **Spectral Morphing** [Premium]: The plug-in allows you to *apply the spectral characteristics of a side-chain sound to a source sound in order to transform its timbre*. By using a voice sound as the side-chain on an instrument sound as the source, spectral morphing can be used to *make the instrument speak*.  
• **Formant Shaping** [Premium]: The plug-in allows you to *modify the vowels and play with the formant resonances* of the sound. It can be used to change spoken vowels of a voice or to *vocalize instruments* such as a drum set.  
• **Spectral Surface** [Premium]: The plug-in allows you to *draw shape filters on the sound spectrogram* and to control their gain and the fades. The sound representation and the effect interface, made possible thanks to the ARA 2 plug-in extension, allow the creation of very *complex and precise surface filters* to reduce or increase specific parts of the spectral components of the sound. The plug-in can be used to *compensate for annoying artifacts* in the sound as well as to *transform the sound creatively*.  
• **Pitches Brew** [Premium]: The Pitches Brew plugin allows you to *transpose the pitch and the formant of sounds by drawing and modifying their frequency curves*. The pitch transposition modifies the fundamental frequency of the sound while preserving its spectral envelope and the formant transposition modifies the spectral envelope of the sound while preserving the fundamental frequency. Based on the ARA extension, the plugin offers a visual representation of the original fundamental frequencies, the expected pitches, and the formants with curves *allowing many original editing such as redrawing, transposing, stretching, copying, and so on*.  
• **Stretch Life** [Premium]: The Stretch Life plugin allows you to stretch and compress sound in creative and original ways. Thanks to an intelligent algorithm, resulting from many years of work and experimentation, the temporal transformations offer an *exceptional rendering quality* by preserving the pitch of the harmonic components of the sound and the random characteristics of the noisy parts. Thanks to the ergonomic interface allowing real-time visualization of the transformations on the spectrogram, the plugin opens up new creative and original possibilities. The many marker editing operations provide fast and efficient solutions for sound engineers to synchronize tracks, resample audio files, and more.  
ㅤ  
![Logos](https://git.forum.ircam.fr/guillot/asap/uploads/5e270fbb189f97bf784f90cf745b36be/Logos.png)

The plug-ins are available in the **VST3**, **Audio Unit** and **AAX** formats (with the **ARA** extension for processing that requires entire audio information) and are compatible with **macOS** 10.13 and higher 64bit (Universal 2 - Intel/Silicon), **Windows** 10 & 11 64bit and **Linux** 64bit.

The Spectral Remix and the Spectral Clipping plug-ins are part of the Ircam Forum free membership. The other ASAP plug-ins set is part of the Ircam [Forum Premium](https://forum.ircam.fr/about/offres-dabonnement/) technologies bundle offering [many tools, plug-ins, applications, and datasets](https://forum.ircam.fr/collections/detail/Technologies-ircam-premium/) to analyze, synthesize, and transform the sound. The Premium subscription gives you access to **downloading and installing Ircam Premium technologies for one year**. Once installed on your machine, you can **use these technologies without time limit**. The [free membership](https://forum.ircam.fr/collections/detail/technologies-ircam-free/) allows you to use the **demo version** of the ASAP plug-ins.

If you have any **requests**, **comments**, or **bug reports**, please create a new thread on the [discussion forum](https://discussion.forum.ircam.fr/).

![Logos](https://git.forum.ircam.fr/guillot/asap/uploads/2ea659af6b958460e28e5d461155bdad/Communication.png)
     
   
**ASAP** is designed and developed by Pierre Guillot at [IRCAM](https://www.ircam.fr) IMR Department.  
**SuperVP** is designed by Axel Röbel (based on an initial version by Philippe Depalle) and developed by Axel Röbel and Frédéric Cornu of the [Analysis-Synthesis team](https://www.stms-lab.fr) of the STMS Lab hosted at IRCAM.  

**Audio Unit PlugIn Technology** by [Apple Computer, Inc.](https://www.apple.com)  
**VST PlugIn Technology** by [Steinberg Media Technologies](https://www.steinberg.net/technology)  
**AAX PlugIn Technology** by [Avid Avid Technology, Inc.](https://www.avid.com/fr/avid-plugins-by-category)  
**ARA SDK** by [Celemony Software GmbH](https://www.celemony.com/en/start) 
     
   

### Videos Gallery
[![ASAP Video Presentations](https://git.forum.ircam.fr/guillot/asap/uploads/e1a51eaeeab48f47a5490070536a579e/Playlist_Title.png)](https://forum.ircam.fr/article/detail/asap-tutorials/)
[![ASAP Vimeo](https://git.forum.ircam.fr/guillot/asap/uploads/3fde10509038f7afe425dafd7fe0834e/Pitches_Brew_Play.png)](https://vimeo.com/showcase/10005314)
[![Introduction](https://git.forum.ircam.fr/guillot/asap/uploads/b30a19ae83ccc00554ee863ded335145/Presentation.png)](https://nubo.ircam.fr/index.php/s/sJTJ9EWHWnqSyNt)
[![Advertising](https://git.forum.ircam.fr/guillot/asap/uploads/d2f5818c7803e0d7326f2c7b93438dd9/ASAP_-_Ad_-_Image_-_Video.png)](https://nubo.ircam.fr/index.php/s/konFyjzW6SEgAaK)

### Images Gallery
![Pitches Brew](https://git.forum.ircam.fr/guillot/asap/uploads/2768ee193024e3cfabb73a2decb40c16/pitches-brew.png)
![Spectral Surface](https://git.forum.ircam.fr/guillot/asap/uploads/762e7cf0a40b118d666e9fa17bc2c7c7/spectral-surface.png)
![Spectral Remix](https://git.forum.ircam.fr/guillot/asap/uploads/76f925b49a95f4492eba101470e30fe7/spectral-remix.png)
![Spectral Crossing](https://git.forum.ircam.fr/guillot/asap/uploads/768a2a160c1ee5b11b7de3e9f0d20318/spectral-crossing.png)
![Spectral Morphing](https://git.forum.ircam.fr/guillot/asap/uploads/8c8674700d5c7ece548c3e160266a42e/spectral-morphing.png)
![Spectral Clipping](https://git.forum.ircam.fr/guillot/asap/uploads/58157d218f7425ad098271c8985c4969/spectral-clipping.png)
![Formant Shaping](https://git.forum.ircam.fr/guillot/asap/uploads/89772f57004cfc705ff52c92b279a8aa/formant-shaping.png)

![Spectral Surface](https://git.forum.ircam.fr/guillot/asap/uploads/529eb6c29a172cc0f504d2c2ade3379e/spectral-surface-b.png)
![Spectral Remix](https://git.forum.ircam.fr/guillot/asap/uploads/9d4057c8c24b60e3f015ab0a66e54d22/spectral-remix-b.png)
![Spectral Crossing](https://git.forum.ircam.fr/guillot/asap/uploads/f207d0dd286794cb82df8afbac85e60b/spectral-morphing-b.png)
![Spectral Morphing](https://git.forum.ircam.fr/guillot/asap/uploads/300ecccc38f332d107a77631c7f8ea21/spectral-crossing-b.png)
![Spectral Clipping](https://git.forum.ircam.fr/guillot/asap/uploads/9b30393e20178c4630c792b9a8d0fd33/spectral-clipping-b.png)
![Formant Shaping](https://git.forum.ircam.fr/guillot/asap/uploads/42625ca70af6ceaa5409d5a37d888581/formant-shaping-b.png)

### News Roundup

[Bed Room Producer - IRCAM Releases Two Free AudioSculpt Plugins](https://bedroomproducersblog.com/2023/01/24/arcam-asap/)  
[KVR - IRCAM updates the ASAP plug-ins collection](https://www.kvraudio.com/news/ircam-updates-the-asap-plug-ins-collection-56903)  
[Rekkerd - Transform sound in a creative way with ASAP by IRCAM](https://rekkerd.org/transform-sound-in-a-creative-way-with-asap-by-ircam/)  
[CDM - IRCAM ASAP plug-in suite transforms and remixes sounds](https://cdm.link/2022/12/ircam-asap-plug-in-suite-transforms-sound/)    
[Future Music Es - Estos seis plugins de IRCAM ASAP transforman el sonido de forma creativa](https://www.futuremusic-es.com/seis-plugins-ircam-asap-transforman-sonido/)   
[Audiofanzine - Mise à jour aussi pour les plug-ins de l'Ircam !](https://fr.audiofanzine.com/plugin-fx-divers/ircam/asap/news/a.play,n.68416.html)    
[Gearspace - ASAP, the new plug-ins collection by IRCAM](https://gearspace.com/board/new-product-alert/1395024-asap-new-plug-ins-collection-ircam.html)    
[VI Control - IRCAM announce ASAP, a new collection of spectral effect plug-ins](https://vi-control.net/community/threads/ircam-announce-asap-a-new-collection-of-spectral-effect-plug-ins.133549/)  
[Elektronauts - Ircam ASAP - plugin suite for spectral sound manipulation](https://www.elektronauts.com/t/ircam-asap-plugin-suite-for-spectral-sound-manipulation/185249)  
[KVR - IRCAM announce ASAP, a new collection of spectral effect plug-ins](https://www.kvraudio.com/news/ircam-announce-asap-a-new-collection-of-spectral-effect-plug-ins-56668)  
[Audiofanzine - L'IRCAM a mis en ligne la suite de plug-ins ASAP](https://fr.audiofanzine.com/plugin-fx-divers/ircam/asap/news/a.play,n.67084.html)  

